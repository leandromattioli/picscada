from enum import Enum
from tkinter import *
from tkinter.ttk import *

#FIXME Use NumericLabel or generalize Label widget?
#TODO Add 'textvariable' ??

class Bases(Enum):
    def __str__(self):
        return self.value
    DEC = 'DEC'
    BIN = 'BIN'
    HEX = 'HEX'

class NumericLabel(Label):
    def __init__(self, master=None, base=Bases.DEC, number=0, **kw):
        Label.__init__(self, master, **kw)
        self._widgetKeys = ( 'number', 'base' )
        self._number = number
        self._base = base
        self._show()
        
    def fmtBin(self, byte):
        """Bases binary data grouping nibbles"""
        bits = bin(byte)[2:]
        size = len(bits)
        if size % 4 > 0:
            size = size + 4 - (size % 4)
        bits = bits.zfill(size)
        msg = ''
        cursorA = 0
        for i in range(size // 4):
            cursorA = i * 4
            cursorB = (i + 1) * 4
            msg += bits[cursorA:cursorB] + ' '
        return '0b ' + msg
        
    def fmtDec(self, byte):
        return str(byte).zfill(3)
        
    def fmtHex(self, byte):
        return '0x' + hex(byte)[2:].upper()
        
    def _show(self):
        if self._base in (Bases.DEC, Bases.DEC.value):
            self['text'] = self.fmtDec(self._number)
        elif self._base  in (Bases.BIN, Bases.BIN.value):
            self['text'] = self.fmtBin(self._number)
        elif self._base  in (Bases.HEX, Bases.HEX.value):
            self['text'] = self.fmtHex(self._number)
            
    #######################################################################            
    # Inherited Methods
    #######################################################################
        
    def __setitem__(self, key, val):
        if key in self._widgetKeys:
            self.__setattr__('_' + key, val)
            self._show()
        else:
            Label.__setitem__(self, key, val)
        
    def __getitem__(self, key):
        if key in self._widgetKeys:
            return self.__getattribute__('_' + key)
        else:
            return Label.__getitem__(self, key)
            
    def config(self, **kw):
        for key in kw:
            if key in self._widgetKeys:
                self[key] = kw[key]
                kw.pop(key, False)
        Label.config(self, **kw)
        self._show()
        
if __name__ == '__main__':
    root= Tk()
    a = NumericLabel(root)
    a.pack()
    a['base'] = Bases.HEX
    a['number'] = 0x0A
    root.mainloop()
