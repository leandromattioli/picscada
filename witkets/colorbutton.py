from tkinter import *
from tkinter.ttk import *
from tkinter.colorchooser import askcolor

class ColorButton(Canvas):
    def __init__(self, master, color='#CCC', textvariable=None, **kw):
        if 'width' not in kw:
            kw['width'] = 25
        if 'height' not in kw:
            kw['height'] = 25
        if not textvariable:
            self._var = StringVar()
        else:
            self._var = textvariable
        Canvas.__init__(self, master, **kw)
        self._var.set(color)
        self._rect = None
        self._redraw()
        self._var.trace('w', self._redraw)
        self.bind('<Button-1>', self._showDialog)
        
    def __setitem__(self, key, val):
        if key == 'color':
            self._var.set(val)
        elif key == 'textvariable':
            self._var = val
            self._var.trace('w', self._redraw)
        else:
            Canvas.__setitem__(self, key, val)
        self._redraw()
        
    def __getitem__(self, key):
        if key == 'color':
            return self._var.get()
        elif key == 'textvariable':
            return selr._var
        else:
            return Canvas.__getitem__(self, key)
            
    def config(self, **kw):
        for key in kw:
            if key == 'color':
                self._var.set(kw[key])
                kw.pop(key, False)
            elif key == 'textvariable':
                self._var = kw[key]
                kw.pop(key, False)                
        Canvas.config(kw)

    def _redraw(self, *args):
        w, h = int(self['width']), int(self['height'])
        if self._rect:
            self.delete(self._rect)
        self._rect = self.create_rectangle(0, 0, w, h, fill=self._var.get())
                
    def _showDialog(self, event=None):
        newColor = askcolor(color=self._var.get())[1]
        if newColor:
            self._var.set(newColor)
            self._redraw()
            
if __name__ == '__main__':
    root = Tk()
    Label(root, text='Color Button --> click it!').pack()
    a = ColorButton(root, color='red')
    var = StringVar()
    a['textvariable'] = var
    var.set('#00F')
    a.pack()
    root.mainloop()
    
    
