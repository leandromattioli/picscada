from tkinter import *
from tkinter.ttk import *

class Tank(Canvas):
    def __init__(self, master=None, colorlow='red', colormedium='blue', 
                 colorhigh='green', levellow=20, levelhigh=80, 
                 number=50, yoffset=0, yscale=1, ystep=20,
                 padl=30, padr=30, padt=10, padb=10, tickfont='"Courier New" 6', 
                 **kw):
        self._widgetKeys = ('colorlow', 'colormedium', 'colorhigh', 
                            'levellow', 'levelhigh', 'number',
                            'padl', 'padr', 'padt', 'padb', 'tickfont',
                            'yoffset', 'yscale', 'ystep')
        if 'width' not in kw:
            kw['width'] = 110
        if 'height' not in kw:
            kw['height'] = 120
        if 'background' not in kw:
            kw['background'] = '#FFF'
        Canvas.__init__(self, master, **kw)
        #Initializing default values
        self._colorlow = colorlow
        self._colormedium = colormedium
        self._colorhigh = colorhigh
        self._number = number
        self._padl = padl
        self._padr = padr
        self._padb = padb
        self._padt = padt
        self._yoffset = yoffset
        self._yscale = yscale
        self._ystep = ystep
        self._levellow = levellow
        self._levelhigh = levelhigh
        self._tickfont = tickfont
        self._h = int(kw['height'])
        #Conversion functions
        self._yw2s = lambda y : self._h - self._padb - (y + self._yoffset) * self._yscale
        self._ys2w = lambda y : (self._h - self._padb - y)  / self._yscale - self._yoffset
        #Canvas related
        self._objects = []
        self._draw()
        
    def yscreen2number(self, yscreen):
        return self._ys2w(yscreen)
        
    #######################################################################            
    # Drawing functions
    #######################################################################
        
    def _redraw(self):
        """Redraws the Tank widget"""
        for o in self._objects:
            self.delete(o)
        self._draw()
        
    def _draw(self):
        """Draws the Tank widget"""
        w, h = int(self['width']), int(self['height'])
        self._h = h
        low = self._yw2s(self._levellow)
        high = self._yw2s(self._levelhigh)
        #Drawing tank
        r = self.create_rectangle(self._padl, self._padt, 
                                   w - self._padr, h - self._padb)
        self._objects.append(r)
        levelY = self._yw2s(self._number)
        #Low level
        r = self.create_rectangle(self._padl + 1, h - self._padb,
                                  w - self._padr, levelY, 
                                  fill=self._colorlow, width=0)
        self._objects.append(r)
        #Medium level
        if self._number > self._levellow:
            r = self.create_rectangle(self._padl + 1, low,
                                      w - self._padr, levelY, 
                                      fill=self._colormedium, width=0)
            self._objects.append(r)
        if self._number > self._levelhigh:
            r = self.create_rectangle(self._padl + 1, high,
                                      w - self._padr, levelY, 
                                      fill=self._colorhigh, width=0)
            self._objects.append(r)
        #Drawing ticks
        y = self._h - self._padb
        while y >= self._padt:
            l = self.create_line(self._padl, y, self._padl + 5, y)
            self._objects.append(l)
            if y != self._h:
                tickY = self._ys2w(y)
                txt = str(int(round(tickY)))
                tick = self.create_text(self._padl - 2, y, anchor='e', text=txt,
                                        font=self._tickfont)
                self._objects.append(tick)
            y -= self._ystep * self._yscale
        #Drawing right tick
        l = self.create_line(w - self._padr, levelY, w - self._padr - 5, levelY)
        self._objects.append(l)
        tickY = self._ys2w(levelY)
        txt = str(int(round(tickY)))
        tick = self.create_text(w - self._padr + 2, levelY, anchor='w', text=txt,
                                font=self._tickfont)
        self._objects.append(tick)
        
    #######################################################################            
    # Inherited Methods
    #######################################################################
        
    def __setitem__(self, key, val):
        if key in self._widgetKeys:
            self.__setattr__('_' + key, val)
        else:
            Canvas.__setitem__(self, key, val)
        self._redraw()
        
    def __getitem__(self, key):
        if key in self._widgetKeys:
            return self.__getattribute__('_' + key)
        else:
            return Canvas.__getitem__(self, key)
            
    def config(self, **kw):
        for key in kw:
            if key in self._widgetKeys:
                self[key] = kw[key]
                kw.pop(key, False)
        Canvas.config(self, **kw)
        self._redraw()

if __name__ == '__main__':
    motion = False
    
    def onClicked(event):
        xrel = event.x_root - event.widget.winfo_rootx()
        yrel = event.y_root - event.widget.winfo_rooty()
        newLevel = event.widget.yscreen2number(yrel)
        newLevel = newLevel if newLevel >= 0 else 0
        newLevel = newLevel if newLevel < 100 else 100
        event.widget['number'] = newLevel
        global motion
        motion = True
        
    def onRelease(event):
        global motion
        motion = False
        
    def onMotion(event):
        global motion
        if not motion:
            return
        xrel = event.x_root - event.widget.winfo_rootx()
        yrel = event.y_root - event.widget.winfo_rooty()
        newLevel = event.widget.yscreen2number(yrel)
        newLevel = newLevel if newLevel >= 0 else 0
        newLevel = newLevel if newLevel < 100 else 100
        event.widget['number'] = newLevel

    root = Tk()
    t = Tank(root)
    t['tickfont'] = '"Courier New" 10'
    t['height'] = 320
    t['width'] = 140
    t['padl'] = 40
    t['padr'] = 40
    t['yscale'] = 3
    t['number'] = 90
    t.bind('<ButtonPress>', onClicked)
    t.bind('<Motion>', onMotion)
    t.bind('<ButtonRelease>', onRelease)
    t.pack()
    root.mainloop()
