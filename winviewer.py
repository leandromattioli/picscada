#!/usr/bin/env python3

import sys
from tkinter import *
from tkinter.ttk import *
from tkinter import messagebox
from tkinter.messagebox import showerror
from tkinter.filedialog import askopenfilename
from tkbuilder.tkbuilder import TkBuilder
from core.theme import Theme
from core.themedlabelframe import ThemedLabelFrame
from core.serialframe import SerialFrame
from core.comm import CommLayer
from witkets.led import LED
from witkets.logicswitch import LogicSwitch
from witkets.scope import Scope
#from witkets.numericlabel import NumericLabel
from editor.document import Document
from editor.library import library
from persistence.docreader import DocReader
try:
    from serial import Serial
    from serial.serialutil import SerialException
    from core.comutils import ComUtils
except ImportError:
    print('Error: Module serial not found!')
    messagebox.showerror('Erro!', 'Biblioteca python3-serial não encontrada')
    sys.exit(-1)

xmlSerial = '''
<root>
    <label wid="lblPort" text="Porta:" />
    <combobox wid="comboPort" />
    <label wid="lblBaud" text="Baud:" /> 
    <combobox wid="comboBaud" />
    <label wid="lblStatus" text="Desconectado" />
    <button wid="btnConnect" text="Conectar" />
    <button wid="btnDisconnect" text="Desconectar" />
    <geometry>
        <pack for="lblPort" side="left" />
        <pack for="comboPort" side="left" />
        <pack for="lblBaud" side="left" />
        <pack for="comboBaud" side="left" />
        <pack for="lblStatus" side="left" />
        <pack for="btnConnect" side="left" />
        <pack for="btnDisconnect" side="left" />
    </geometry>
</root>
'''

class WinViewer:
    def __init__(self, root=None, filename='screen.ui'):
        self.root = root
        if not root:
            self.root = Tk()
        self.root.configure(padx=5, pady=5)
        self.root.title('WinViewer')
        #Serial communication
        self._configCommunications()
        #Base GUI
        self.frame = Frame(self.root)
        self.canvas = Canvas(self.frame)
        self.canvas['bg'] = '#FFF'
        self.canvas.pack()
        self.frame.pack()
        #Dynamic GUI
        self.doc = Document.fromFile(self.frame, filename)
        self.doc.pack()
        self.canvas['width'] = self.frame['width']
        self.canvas['height'] = self.frame['height']
        try:
            imgFile = getattr(self.frame, 'bgimg')
            self.photoimg = PhotoImage(file=imgFile)
            self.img = self.canvas.create_image(0, 0, image=self.photoimg, 
                                                anchor='nw')
        except AttributeError:
            pass
        #I/O Config
        self._configDigitalInput()
        self._configAnalogInput()
        self._configDigitalOutput()
        self._configAnalogOutput()
        #Style and minimum size constraint
        self.setStyle()
        self.root.update()
        self.root.minsize(self.root.winfo_width(), self.root.winfo_height())
        
    def run(self):
        self.root.mainloop()
        
    def setStyle(self):
        self.root['bg'] = '#FFF'
        Theme.applyTheme(Style(), filepath='data/styles.ini')
        
    # =========================================================================
    # Serial Communication
    # =========================================================================
    
    def _configCommunications(self):
        """RS-232 Configuration"""
        #Porta
        self.frmSerial = Frame(self.root)
        builder = TkBuilder(self.frmSerial)
        builder.buildString(xmlSerial)
        self.varPort = StringVar()
        self.comboPort = builder.nodes['comboPort']
        self.comboPort['textvariable'] = self.varPort
        self.comboPort['values'] = ComUtils.listSerialPorts()
        if len(sys.argv) >= 3:
            self.varPort.set(sys.argv[2])
        elif self.comboPort['values']:
            self.varPort.set(self.comboPort['values'][0])
        self.varBaud = IntVar()
        self.comboBaud = builder.nodes['comboBaud']
        self.comboBaud['textvariable'] = self.varBaud
        self.comboBaud['values'] = [ 2400, 4800, 9600, 19200, 57600, 115200 ]
        self.varBaud.set(9600)
        self.varStatus = StringVar()
        self.varStatus.set("Desconectado.")
        self.lblStatus = builder.nodes['lblStatus']
        self.lblStatus['textvariable'] = self.varStatus
        self.btnConnect = builder.nodes['btnConnect']
        self.btnConnect['command'] = self._connect
        self.btnDisconnect = builder.nodes['btnDisconnect']
        self.btnDisconnect['command'] = self._disconnect
        self.frmSerial.pack()
        #Comm layer
        self._comm = CommLayer(Serial())
        self._updateSensivity()
        self.root.after(100, self._receiveTask)
        
    def _connect(self):
        self._comm.serial.port = self.varPort.get()
        self._comm.serial.baudrate = self.varBaud.get()
        try:
            self._comm.serial.open()
        except SerialException:
            messagebox.showerror("Erro", "Falha de comunicação")
        self._updateSensivity()
        
    def _disconnect(self):
        if self._comm.serial.isOpen():
            self._comm.serial.close()
        self._updateSensivity()
        
    def _updateSensivity(self):
        if self._comm.serial.isOpen():
            self.btnConnect['state'] = 'disabled'
            self.btnDisconnect['state'] = 'normal'
            self.varStatus.set("Conectado.")
            self.lblStatus['style'] = "Connected.TLabel"
        else:
            self.btnConnect['state'] = 'normal'
            self.btnDisconnect['state'] = 'disabled'
            self.varStatus.set("Desconectado.")
            self.lblStatus['style'] = "Disconnected.TLabel"
        
    def _receiveTask(self):
        if self._comm.serial.isOpen():
            msg = self._comm.getmsg()
            if msg:
                if msg[0] == 0x1B:
                    self._onDigitalReceive(msg[1])
                if msg[2] == 0x1A:
                    self._onAnalogReceive(msg[3:])
                else:
                    print('Mensagem inválida %s' % str(msg))
            self.root.after(10, self._receiveTask)
        else:
            self.root.after(100, self._receiveTask)
            
    # =========================================================================
    # Digital Input
    # =========================================================================
    
    def _configDigitalInput(self):
        self._varDigIn = IntVar()
        self._digInBitWidgets = []  #bit-oriented widgets
        self._digInByteWidgets = [] #byte-oriented widgets
        for i in range(8):
            self._digInBitWidgets.append([])
        for wid in self.doc:
            if hasattr(wid, 'prop_inpin'):
                pin = int(wid.prop_inpin)
                self._digInBitWidgets[pin].append(wid)
            elif hasattr(wid, 'prop_inchs') and wid.prop_inchs == 'digital':
                self._digInByteWidgets.append(wid)
                #wid['textvariable'] = self._varDigIn
                
    def _onDigitalReceive(self, newByte):
        #Update byte-oriented widgets
        self._varDigIn.set(newByte)
        for widget in self._digInByteWidgets:
            if widget.__class__ == Scope:
                widget.plot.addPoint((self._x, newByte))
                widget.plot.redraw()
            else:
                    wid['number'] = newByte
        #Update bit-oriented widgets
        for pin in range(8):
            newState = newByte & (1 << pin)
            for wid in self._digInBitWidgets[pin]:
                wid['boolean'] = newState
        
    # =========================================================================
    # Analog Input
    # =========================================================================
    
    def _configAnalogInput(self):
        self._varsAnIn = []
        self._x = 0
        self._anInput = []
        types = ['an0', 'an1', 'an2', 'an3', 'an4', 'an5', 'an6', 'an7']
        for i in range(8):
            self._anInput.append([])
            self._varsAnIn.append(IntVar())
        for wid in self.doc:
            if hasattr(wid, 'prop_inchs') and wid.prop_inchs in types:
                channel = int(wid.prop_inchs[2])
                self._anInput[channel].append(wid)
                    
    def _onAnalogReceive(self, msg):
        for i in range(8):
            h = 2*i
            l = 2*i + 1
            newValue = (msg[h] << 8) + msg[l]
            self._varsAnIn[i].set(newValue)
            for wid in self._anInput[i]:
                if wid.__class__ == Scope:
                    wid.plot.addPoint((self._x, newValue))
                    wid.plot.redraw()
                else:
                    wid['number'] = newValue
            self._x += 1
        
    # =========================================================================
    # Digital Output
    # =========================================================================
    
    def _configDigitalOutput(self):
        self._traceIgnore = False
        self._varDigOut = IntVar()
        self._varDigOut.trace('w', self._onDigOutChange)
        self._digOutBitWidgets = []
        for i in range(8):
            self._digOutBitWidgets.append([])
        for widget in self.doc:
            if hasattr(widget, 'prop_outpin'):
                pin = int(widget.prop_outpin)
                self._digOutBitWidgets[pin].append(widget)
                widget.bind('<Button-1>', self._onSwitchClick)
            elif hasattr(widget, 'prop_entrytype') and \
                widget.prop_entrytype == 'digital':
                widget['textvariable'] = self._varDigOut
                widget.bind('<Return>', self._onDigOutChange)
                widget.bind('<KP_Enter>', self._onDigOutChange)
            #Button
            elif hasattr(widget, 'action') and widget['action'] == 'senddigital':
                wid.bind('<Button-1>', self._sendDigital)
                
    def _onDigOutChange(self):
        return
            
    def _onSwitchClick(self, event):
        #Is this needed?? Switches will be updated anyway
        widget = event.widget
        pin = int(widget.prop_outpin)
        widget.toggle()
        newState = widget['boolean']
        for i in self._digOutBitWidgets[pin]:
            i['boolean'] = newState
        if newState:
            self._varDigOut |= (1 << pin)
        else:
            self._varDigout &= ~(1 << pin)
            
    def _sendDigital(self):
        if not self._comm.serial.isOpen():
            messagebox.showerror("Erro", "Não conectado!")
            return
        #Constructing value from switches
        val = 0
        for i in range(8):
            if self._digOutBitWidgets[i]:
                wid = self._digOutBitWidgets[i][0] #any switch
                if wid['boolean']:
                    val |= (1 << i)
        try:
            self._comm.send(bytes([0x0D, val, 0xDF]))
        except:
            messagebox.showerror("Erro", "Erro na comunicação!\n" + e.message())
            
    # =========================================================================
    # Analog Output
    # =========================================================================
    
    def _configAnalogOutput(self):
        return
        self.anVars = []
        for i in range(3):
            self.anVars.append(IntVar())
            self.spins[i]['textvariable'] = self.anVars[i]
            self.anVars[i].set('127')  
        for wid in self.doc:
            if hasattr(wid, 'prop_outchs'):
                pin = int(wid.prop_outpin)
                self._digOutBitWidgets[pin].append(wid)
                wid.bind('<Button-1>', self._onSwitchClick)
            elif hasattr(wid, 'action') and wid['action'] == 'senddigital':
                wid.bind('<Button-1>', self._sendDigital)
    
    def _sendAnalog(self):
        return
        values = [0x0A]
        for i in range(3):
            values.append(int(self.anVars[i].get()))
        values.append(0xAF)
        self._comm.send(values)
        
        
if __name__ == '__main__':
    root = Tk()
    options = {}
    options['filetypes'] = [('WinMaker ', '.ui')]
    options['title'] = 'Abrir tela...'
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = askopenfilename(**options)
    if filename:
        app = WinViewer(root, filename)
        app.run()
