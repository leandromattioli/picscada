#!/usr/bin/env python3

import sys
import xml.etree.ElementTree as ET
from tkinter import *
from tkinter.ttk import *
from tkinter.scrolledtext import ScrolledText
from copy import copy

tag2tk = {
	#TK Base
    'button': Button,
	'canvas': Canvas,
	'checkbutton': Checkbutton,
	'entry': Entry,
    'frame': Frame,
    'label': Label,
	'labelframe': LabelFrame,
	'listbox': Listbox,
	'menu': Menu,
	'menubutton': Menubutton,
	'message': Message,
	'optionmenu': OptionMenu,
	'panedwindow': PanedWindow,
	'radiobutton': Radiobutton,
	'scale': Scale,
	'scrollbar': Scrollbar,
	'scrolledtext': ScrolledText,
	'spinbox': Spinbox,
	'text': Text,
	#TTK
	'combobox': Combobox,
	'notebook': Notebook,
	'progressbar': Progressbar,
	'separator': Separator,
	'sizegrip': Sizegrip,
	'treeview': Treeview,
}

containers = [ 'root', 'frame' ]

class TkBuilder:
    def __init__(self, master):
        self._tree = None
        self._root = None
        self._master = master
        self.nodes = {}
        
    def addTag(self, tag, className, container=False):
        """Maps a tag to a class"""
        tag2tk[tag] = className
        if container:
            containers.append(tag)
        
    def _handleWidget(self, widgetNode, parent):
        """Handles individual widgets tags"""
        try:
            wid = widgetNode.attrib.pop('wid')
        except KeyError:
            print('Required key "wid" not found in %s' % widgetNode.tag, sys.stderr)
            return
        # Creating widget        
        tkClass = tag2tk[widgetNode.tag]
        if parent == self._root:
            parentNode = self._master
        else:
            parentWid = parent.attrib['wid']
            parentNode = self.nodes[parentWid]        
        self.nodes[wid] = tkClass(parentNode)
        # Mapping attributes
        self._handleAttributes(self.nodes[wid], widgetNode.attrib)
    
    def _handleAttributes(self, widget, attribs):
        """Handles attributes, except TkBuilder related"""
        for key,val in attribs.items():            
            try:
                widget[key] = val #fails for Bool
            except KeyError:
                print('[warning] Invalid key "%s"' % key)
        
    def _handleContainer(self, container, parent):
        """Handles containers (<root>, <frame> and user-defined containers)"""
        if container != self._root:    
            try:
                attribs = copy(container.attrib)
                wid = attribs.pop('wid')
                tkClass = tag2tk[container.tag]
                if parent != self._root:
                    parentWid = parent.attrib['wid']
                    parentNode = self.nodes['wid']
                else:
                    parentNode = self._master
                self.nodes[wid] = tkClass(parentNode)
                self._handleAttributes(self.nodes[wid], attribs)
            except KeyError:
                print('Required key "wid" not found in %s' % container.tag, sys.stderr)
                return
        for child in container:
            if child.tag in containers:
                self._handleContainer(child, container)
            elif child.tag == 'geometry':
                self.currParent = container
                self._handleGeometry(child)
            elif child.tag in tag2tk.keys():
                self._handleWidget(child, container)
            else:
                print('Invalid tag: %s!' % child.tag, sys.stderr)
        if container == self._root:
            attribs = container.attrib
            self._handleAttributes(self._master, attribs)
        
    def _handleGeometry(self, geometry):
        """Handles the special <geometry> tag"""
        for child in geometry:
            attribs = copy(child.attrib)
            if child.tag in ('pack', 'grid', 'place'):
                # Getting widget ID
                try:
                    wid = attribs.pop('for')
                except KeyError:
                    #@TODO emit error
                    print('[geom] Required key "for" not found in %s' % child.tag, sys.stderr)
                    continue
                # Calling appropriate geometry method
                if wid not in self.nodes:
                    print(self.nodes)
                if child.tag == 'pack':
                    self.nodes[wid].pack(**attribs)
                elif child.tag == 'grid':
                    self.nodes[wid].grid(**attribs)
                elif child.tag == 'place':
                    self.nodes[wid].place(**attribs)
            else:
                print('Invalid geometry instruction %s' % child.tag, sys.stderr)    
                #@TODO emit error
                continue

    def _parseTree(self):
        """Parses XML and builds interface"""
        if self._root.tag != 'root':
            msg = 'Invalid root tag! Expecting "root", but found %s'
            print(msg % self._root.tag, sys.stderr)
            return False
        self._handleContainer(self._root, self._master)
        return True

    def buildFile(self, filepath):
        """Build XML from file"""
        self._tree = ET.parse(filepath)
        self._root = self._tree.getroot()
        self._parseTree()

    def buildString(self, contents):
        """Build XML from string"""
        self._root = ET.fromstring(contents)
        self._parseTree()
    
if __name__ == '__main__':
    example = '''
        <root>
            <label wid="lbl1" text="Teste" background="red" />
            <frame wid="frm1">
                <label wid="lbl2" text="Teste 2" />
                <label wid="lbl3" text="Teste 3" />
                <geometry>
                    <grid for="lbl2" row="0" column="0" sticky="w" />
                    <grid for="lbl3" row="0" column="2" sticky="e"/>
                </geometry>
            </frame>
            <geometry>
                <pack for="lbl1" fill="y" expand="1" />
                <pack for="frm1" />
            </geometry>
        </root>
'''

    root = Tk()
    builder = TkBuilder(root)
    builder.buildString(example)
    root.mainloop()
