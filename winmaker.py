#!/usr/bin/env python3

import sys

from tkinter import *
from tkinter.ttk import *
from tkinter import messagebox
from tkinter.messagebox import askokcancel

from core.theme import Theme

from witkets.led import LED
from witkets.logicswitch import LogicSwitch
from witkets.scope import Scope
from witkets.toolbar import Toolbar

from editor.library import library
from editor.propertyeditor import PropertyEditor
from editor.docwindow import DocWindow
from editor.document import Document

from persistence.docwriter import DocWriter
from persistence.docwriter import DocWriter

class WinMaker:
    def __init__(self):
        self._root = Tk()
        self._root.configure(padx=5, pady=5)
        self._root.title('Criador de Telas')
        self._root.protocol('WM_DELETE_WINDOW', self._onClose)
        self._setStyle()
        self._createSidebar()
        #Core
        self._drag = None
        self._docwins = []
        self._docwins.append(DocWindow(self, self._root))
        if len(sys.argv) > 1:
            self._docwins[0].open(sys.argv[1])
        
    def run(self):
        self._root.mainloop()
        
    def _onClose(self):
        if self._docwins[0].confirmDiscard():
            sys.exit()
        
    def _setStyle(self):
        self._root['bg'] = '#FFF'
        Theme.applyTheme(Style(), filepath='data/styles.ini')
        
    #==========================================================================
    # Sidebar
    #==========================================================================
        
    def _createSidebar(self):
        """Creates sidebar widgets"""
        self._sidebar = Frame(self._root)
        self._vscroll = Scrollbar(self._root, orient='vertical')
        self._sidebar.pack(side='left', fill=BOTH, expand=1)
        self._vscroll.pack(side='right', fill=Y)
        self._populateSidebar()
        
    def _populateSidebar(self):
        """Insere objetos da biblioteca na paleta"""
        row = 0
        for cls, o in library.items():
            wid = cls(self._sidebar)
            for p in o.properties:
                if not p.name.startswith('prop_'):
                    wid[p.name] = p.defval
            label = Label(self._sidebar, text=o.label)
            wid.grid(row=row, column=0, sticky='e')
            label.grid(row=row, column=1, sticky='we')
            sep = Separator(self._sidebar, orient=HORIZONTAL)
            sep.grid(row=row+1, column=0, columnspan=2, pady=5, sticky='ew')
            #label.bind('<ButtonPress>', self.onSidebarPress)
            wid.bind('<ButtonPress>', self._onSidebarPress)
            row += 2
            
    #==========================================================================
    # Drag and Drop
    #==========================================================================
    
    def _onSidebarPress(self, event):
        """Início do drag-and-drop pela paleta"""
        self._drag = library[event.widget.__class__]

    #==========================================================================
    # Properties
    #==========================================================================
    
    @property
    def drag(self):
        return self._drag
        
    @drag.setter
    def drag(self, val):
        self._drag = val
        
            
if __name__ == '__main__':
    app = WinMaker()
    app.run()
