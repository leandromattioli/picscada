import sys
from core.types import *
from editor.library import library

class DocWriter:
    def __init__(self, doc):
        self._doc = doc
        
    def _dumpchild(self, child):
        cls = (child.__class__)
        item = library[cls]
        attribs = ''
        for p in item.properties:
            if p.name.startswith('prop_'):
                val = getattr(child, p.name)
                name = 'custom_' + p.name 
            else:
                val = child[p.name]
                name = p.name
            if type(val) == bool:
                val = '1' if val else '0'
            attribs += "%s='%s' " % (name, val)
        self._xml += '<%s wid="child%s" %s />' % (item.name, str(self._idx), attribs)
        self._idx += 1
        
    def _dumpgeometry(self, child):
        x = child.winfo_x()
        y = child.winfo_y()
        msg = '<place for="child%s" x="%s" y="%s" anchor="nw" />'
        self._xml += msg % (str(self._idx), x, y)
        self._idx += 1
    
    def dump(self, file=sys.stdout):
        children = [x for x in self._doc.winfo_children() 
                      if x.__class__ in library]
        vals = (self._doc['width'], self._doc['height'], self._doc.prop_bgimg,
                self._doc.prop_gridstepx, self._doc.prop_gridstepy, 
                self._doc.prop_gridshow, self._doc.prop_gridsnap)
        attribs = ['width', 'height', 'custom_prop_bgimg', 
                   'custom_prop_gridstepx', 'custom_prop_gridstepy', 
                   'custom_prop_gridshow', 'custom_prop_gridsnap']
        attribs = [ x + '="%s"' for x in attribs ]
        msg  = '<root '+ ' '.join(attribs) + '>'
        self._xml =  msg % vals
        if children:
            self._idx = 0
            for c in children:
                self._dumpchild(c)
            self._idx = 0
            self._xml += '<geometry>'
            for c in children:
                self._dumpgeometry(c)
            self._xml += '</geometry>'
        self._xml += '</root>'
        file.write(self._xml)
