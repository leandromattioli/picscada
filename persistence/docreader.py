#!/usr/bin/env python3

import sys
import xml.etree.ElementTree as ET
from tkinter import *
from tkinter.ttk import *
from tkinter.scrolledtext import ScrolledText
from copy import copy

from witkets.numericlabel import Bases

from tkbuilder.tkbuilder import TkBuilder
from core.types import *
from editor.library import library

tag2tk = {
	#TK Base
    'button': Button,
	'canvas': Canvas,
	'checkbutton': Checkbutton,
	'entry': Entry,
    'frame': Frame,
    'label': Label,
	'labelframe': LabelFrame,
	'listbox': Listbox,
	'menu': Menu,
	'menubutton': Menubutton,
	'message': Message,
	'optionmenu': OptionMenu,
	'panedwindow': PanedWindow,
	'radiobutton': Radiobutton,
	'scale': Scale,
	'scrollbar': Scrollbar,
	'scrolledtext': ScrolledText,
	'spinbox': Spinbox,
	'text': Text,
	#TTK
	'combobox': Combobox,
	'notebook': Notebook,
	'progressbar': Progressbar,
	'separator': Separator,
	'sizegrip': Sizegrip,
	'treeview': Treeview,
}

containers = [ 'root', 'frame' ]

class DocReader(TkBuilder):
    def __init__(self, master=None):
        TkBuilder.__init__(self, master)
        for k,v in library.items():
            self.addTag(v.name, k)
        
    def _getTypedValue(self, properties, key, val):
        key = key.replace('custom_', '')
        p = [p for p in properties if p.name==key]
        if not p:
            return val
        p = p[0]
        typename = p.typename
        if p.typename == bool:
            return True if val=='1' else False
        elif p.typename in [pin, uint8, uint16, int8, int16]:
            return int(val)
        elif p.typename == float:
            return float(val)
        elif p.typename == Bases:
            return val
        else:
            return val
        
    def _handleAttributes(self, widget, attribs):
        """Handles attributes, except TkBuilder related"""
        cls = widget.__class__
        properties = []
        if cls in library:
            properties = library[cls].properties
        elif cls == self._master.__class__: #Document class
            properties = widget.properties
        for key, val in attribs.items():
            val = self._getTypedValue(properties, key, val)
            if not key.startswith('custom_'):
                widget[key] = val
            else:
                setattr(widget, key.replace('custom_', ''), val)
        

if __name__ == '__main__':
    example = '''
        <root>
            <label wid="lbl1" text="Teste" background="red" />
            <frame wid="frm1">
                <label wid="lbl2" text="Teste 2" />
                <label wid="lbl3" text="Teste 3" />
                <geometry>
                    <grid for="lbl2" row="0" column="0" sticky="w" />
                    <grid for="lbl3" row="0" column="2" sticky="e"/>
                </geometry>
            </frame>
            <geometry>
                <pack for="lbl1" fill="y" expand="1" />
                <pack for="frm1" />
            </geometry>
        </root>
'''

    root = Tk()
    builder = DocReader(root)
    builder.buildString(example)
    root.mainloop()
