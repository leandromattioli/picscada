#!/usr/bin/env python3

from tkinter import *
from tkinter import font
from tkinter.ttk import *
import ast
import configparser

class Theme:
    #http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/ttk-map.html

    @staticmethod
    def applyTheme(style=None, filepath=None, basetheme='clam'):
        style = style if style else Style()
        if basetheme:
            style.theme_use(basetheme)
            
        #Changing default fonts
        defaultFont = font.nametofont("TkDefaultFont")
        defaultFont.configure(size=11, family="Helvetica")
        textFont = font.nametofont("TkTextFont")
        textFont.configure(size=11, family="Helvetica")
        fixedFont = font.nametofont("TkFixedFont")
        fixedFont.configure(size=11, family="Helvetica")
        
        if not filepath:
            return
            
        config = configparser.ConfigParser()
        config.read(filepath)

        style.configure('.', background='#FFF')
        
        for s in config.sections():
            #Maps
            if s.endswith("@MAP"):
                styleName = s[:-4]
                values = {}
                for composedKey in config[s]:
                    parts = composedKey.split('.')
                    #Dictionary keys: foreground=[], background=[], ...
                    key = parts[0]
                    if key not in values:
                        values[key] = []
                    #Constructing list [constraint1, constraint2, ..., value]
                    element = [x.replace('_', '!') for x in parts[1:]]
                    element.append(config[s][composedKey])
                    values[key].append(element)
                style.map(styleName, **values)
            #Ordinary values
            else:
                values = {}
                for key in config[s]:
                    values[key] = config[s][key]
                style.configure(s, **values)
         
if __name__ == '__main__':
    root = Tk()
    root.title('entry widget')
    Label (text='TITLE', style="BlkTitle.TLabel").pack(side=TOP,padx=10,pady=10)
    Entry(root, width=10).pack(side=TOP,padx=10,pady=10)
    Button(root, text='open').pack(side= LEFT)
    Button(root, text='edit').pack(side= LEFT)
    Button(root, text='exit').pack(side= RIGHT)
    Button(root, text='close').pack(side= RIGHT)
    Theme.applyTheme(Style())
    root.mainloop()
