class Observable:
    def __init__(self):
        self.observers = []
    
    def subscribeObserver(self, o):
        self.observers.append(o)
        
    def unsubscribeObserver(self, o):
        self.observers.remove(o)
        
    def notifyObservers(self):
        for o in self.observers:
            o.onNotify(self)
