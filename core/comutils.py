import os
import re
import serial
from serial.tools import list_ports

def natural_key(string_):
    """Natural sorting key comparison"""
    #http://www.codinghorror.com/blog/archives/001018.html
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_)]

class ComUtils:
    @staticmethod
    def listSerialPorts():
        """Retorna uma os nomes das portas de comunicação serial"""
        # Windows
        if os.name == 'nt':
            # Scan for available ports.
            available = []
            for i in range(256):
                try:
                    s = serial.Serial(i)
                    available.append('COM'+str(i + 1))
                    s.close()
                except serial.SerialException:
                    pass
            return available
        else:
            # Mac / Linux
            #ports = [port[0] for port in list_ports.comports()]
            ports = []
            ports.append('/dev/pts/0')
            ports.append('/dev/pts/14')
            ports.append('/dev/pts/15')
            ports.sort(key=natural_key)
            return ports
