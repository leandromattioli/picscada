#!/usr/bin/env python3

from tkinter import *
from tkinter import messagebox
from tkinter.ttk import *

class ThemedLabelFrame(LabelFrame):
    def __init__(self, master=None, title=' ', **kw):
        self.varTitle = StringVar()
        self.varTitle.set(title)
        self.lblTitle = Label(master, textvariable=self.varTitle)
        self.lblTitle['style'] = "BlkTitle.TLabel"
        LabelFrame.__init__(self, master, labelwidget=self.lblTitle, **kw)
        self['style'] = 'Blk.TFrame'
    
    def __setitem__(self, key, val):
        if key == 'title':
            self.varTitle.set(val)
        else:
            LabelFrame.__setitem__(self, key, val)
        
if __name__ == '__main__':
    root = Tk()
    frm = LabeledFrame(root)
    frm['title'] = 'Test'
    btn = Button(frm, text='test')
    btn.pack()
    frm.pack(padx=15, pady=15)
    root.mainloop()
