#!/usr/bin/env python3

from tkinter import *
from tkinter import messagebox
from tkinter.ttk import *

try:
    from core.themedlabelframe import ThemedLabelFrame
    from core.comutils import ComUtils
    from core.theme import Theme
    from core.observable import Observable
except ImportError:
    from themedlabelframe import ThemedLabelFrame
    from comutils import ComUtils
    from theme import Theme
    from observable import Observable
    

import serial

#TODO Update ports

class SerialFrame(ThemedLabelFrame, Observable):
    def __init__(self, master, **kw):
        """Construtor"""
        ThemedLabelFrame.__init__(self, master, 'Comunicação', **kw)
        Observable.__init__(self)
        self.addWidgets()
        self.serial = serial.Serial()
        self.serial.baudrate = 9600
        self.updateSensivity()
        
    def addWidgets(self):
        """Adiciona os widgets na interface"""
        #Porta
        self.lblPorta = Label(self, text="Porta:")
        self.lblPorta.grid(row=0, column=0, sticky='e')
        self.varPort = StringVar()
        self.comboPort = Combobox(self, textvariable=self.varPort)
        self.comboPort['values'] = ComUtils.listSerialPorts()
        if self.comboPort['values']:
            self.varPort.set(self.comboPort['values'][0])
        self.comboPort.grid(row=0, column=1, pady=5, ipadx=10, columnspan=3)
        
        #Baud Rade
        self.lblBaud = Label(self, text="Baud Rate:")
        self.lblBaud.grid(row=1, column=0)
        self.varBaud = IntVar()
        self.comboBaud = Combobox(self, textvariable=self.varBaud)
        self.comboBaud['values'] = [ 2400, 4800, 9600, 19200, 57600, 115200 ]
        self.varBaud.set(9600)
        self.comboBaud.grid(row=1, column=1, pady=5, ipadx=10, columnspan=3)

        #Status
        self.varStatus = StringVar()
        self.lblStatus = Label(self, textvariable=self.varStatus)
        self.lblStatus.grid(row=2, column=1, columnspan=3, padx=0, ipadx=0)

        #Botões
        self.btnConnect = Button(self, text="Conectar")
        self.btnConnect['command'] = self.connect
        self.btnConnect.grid(row=3, column=1, padx=3)
        self.btnDisconnect = Button(self, text="Desconectar") 
        self.btnDisconnect['command'] =self.disconnect
        self.btnDisconnect.grid(row=3, column=3)

    def connect(self):
        self.serial.port = self.varPort.get()
        self.serial.baudrate = self.varBaud.get()
        try:
            self.serial.open()
            self.notifyObservers()
        except serial.serialutil.SerialException:
            messagebox.showerror("Erro", "Falha de comunicação")
        self.updateSensivity()
        
    def disconnect(self):
        if self.serial.isOpen():
            self.serial.close()
            self.notifyObservers()
        self.updateSensivity()
            
    def updateSensivity(self):
        if self.serial.isOpen():
            self.btnConnect.config(state='disabled')
            self.btnDisconnect.config(state='normal')
            self.varStatus.set("Conectado.")
            self.lblStatus['style'] = "Connected.TLabel"
        else:
            self.btnConnect.config(state='normal')
            self.btnDisconnect.config(state='disabled')
            self.varStatus.set("Não conectado.")
            self.lblStatus['style'] = "Disconnected.TLabel"

if __name__ == "__main__":
    root = Tk()
    a = SerialFrame(root)
    a.pack()
    Theme.applyTheme(Style(), filepath='../data/styles.ini')
    root.mainloop()
    input('Pronto.')
