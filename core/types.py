class pin(int):
    name = 'pin'

class uint8(int):
    name = 'uint8'

class uint16(int):
    name = 'uint16'
    
class int8(int):
    name = 'int8'

class int16(int):
    name = 'int16'

class image(str):
    name = 'image'

class color(str):
    name = 'color'

#Enumerations

class action(str):
    name = 'action'

class inchs(str):
    name = 'inchs'

class outchs(str):
    name = 'outchs'

class direction(str):
    name = 'direction'
