#!/usr/bin/env python3

START = 0x12
STOP  = 0x13
ESC   = 0x27

'''
Protocol:
    Input (Data Acquisition): 
    START 0x1B PORTB 0x1A AN0H AN0L AN1H AN1L ... AN7H AN7L STOP
    
    Output (Command & Control):
    START 0x0D PORTD 0xDF STOP
    START 0x0A PWM0 PWM1 PWM2 0xAF STOP
'''

class CommLayer:
    def __init__(self, serial):
        self.serial = serial
        self.serial.timeout = 0.01
    
    def _getsinglebyte(self):
        b = list(self.serial.read())
        if b:
            return b[0]
        else:
            return []
        
    def send(self, message):
        values = [START]
        for b in message:
            if b in (START, STOP, ESC):
                values.append(ESC)
            values.append(b)
        values.append(STOP)
        self.serial.write(bytes(values))
        
    def getmsg(self):
        start = self._getsinglebyte()
        if start != START:
            return None
        values = []
        while True:
            newByte = self._getsinglebyte()
            if newByte == []:
                return None                        #no data received
            elif newByte == ESC:                   #escaping value
                newByte = self._getsinglebyte()    #skipping ESC
            elif newByte == START:                 #flow chars without escaping
                return None
            elif newByte == STOP:
                return values
            values.append(newByte)
