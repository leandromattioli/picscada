from tkinter import *
from tkinter.ttk import *
from tkinter.messagebox import askokcancel
from tkinter.filedialog import askopenfilename, asksaveasfilename

from witkets.toolbar import Toolbar

from editor.propertyeditor import PropertyEditor
from editor.document import Document
from editor.library import library

class DocWindow(Toplevel):

    def __init__(self, app, master=None, **kw):
        Toplevel.__init__(self, master, **kw)
        self._initToolbar()
        self._initEvents()
        self._app = app
        self._doc = None
        self._highlight = None  #rectangle for highlighting purposes
        self._select = None     #selected object
        self._rsel = None       #selected object (right click)
        self._createNew( Document(self, width=640, height=480) )
        self._popupmenu = Menu(master, tearoff=0)
        self._popupmenu.add_command(
            label="Propriedades", 
            command=lambda : self._onDoubleClick(widget=self._rsel))
        self._popupmenu.add_command(
            label="Deletar", 
            command=lambda : self._removeWidget(self._rsel))
    
    #=====================================================================
    # Init
    #=====================================================================    

    def _initEvents(self):
        """Configura eventos da janela"""
        self.protocol('WM_DELETE_WINDOW', self._onClose)
        self.bind('<Alt-F4>', self._onClose)
        self.bind('<Control-n>', self._onNew)
        self.bind('<Control-s>', self._onSave)
        self.bind('<Control-Shift-s>', self._onSaveAs)
        self.bind('<Alt-Return>', self._onSetup)
        
    def _initToolbar(self):
        """Adiciona a barra de ferramentas à janela"""
        self._toolbar = Toolbar(self)
        for i in [('img/icons/document-new.png', self._onNew), 
                  ('img/icons/document-open.png', self._onOpen), 
                  ('img/icons/document-save.png', self._onSave),
                  ('img/icons/document-save-as.png', self._onSaveAs),
                  ('img/icons/document-page-setup.png', self._onSetup)]:
            self._toolbar.addButton(i[0], i[1])
        self._toolbar.pack()
        
    #=====================================================================
    # Public API
    #=====================================================================
    
    def open(self, filename):
        """Abre um arquivo para edição"""
        doc = Document.fromFile(self, filename)
        for obj in doc:
            self._configWidgetEvents(obj)
        self._createNew(doc)
    
    def confirmDiscard(self, parent=None):
        """Solicita confirmação caso o documento tenha alterações"""
        msg = "Descartar alterações?" 
        if parent is None:
            parent = self
        return self._doc.saved or askokcancel("Sair?", msg, parent=parent)

    #=====================================================================
    # Window Title
    #=====================================================================

    def _updateTitle(self):
        msg = 'Sem título 1' if not self._doc.filename else self._doc.filename
        if not self._doc.saved:
            msg += '*'
        self.title(msg)
    
    #=====================================================================
    # Toolbar basic actions
    #=====================================================================

    def _onNew(self, event=None):
        """Verifica alterações pendentes e cria um novo documento"""
        if not self.confirmDiscard():
            return
        self._createNew( Document(self, width=640, height=480) )
            
    def _createNew(self, newDoc):
        """Cria um novo documento"""
        if self._doc:
            self._doc.destroy()
        self._doc = newDoc
        self._doc.canvas.bind_all('<ButtonRelease-1>', self._onButtonRelease)
        self._doc.canvas.bind_all('<Motion>', self._onMotion)
        self._doc.pack()
        self._updateTitle()
        
    def _onOpen(self, event=None):
        """Abre um documento"""
        if not self.confirmDiscard():
            return
        options = {}
        options['filetypes'] = [('WinMaker ', '.ui')]
        options['title'] = 'Abrir'
        options['parent'] = self
        filename = askopenfilename(**options)
        if filename:
            self.open(filename)
        
    def _onSave(self, event=None):
        """Salva o documento atual"""
        if not self._doc.filename: #first time save
            self._onSaveAs()
            return
        self._doc.save()
        self._updateTitle()

    def _onSaveAs(self, event=None):
        """Exibe o diálogo salvar como e salva o documento"""
        options = {}
        options['defaultextension'] = '.ui'
        options['filetypes'] = [('WinMaker ', '.ui')]
        options['title'] = 'Salvar como...'
        options['parent'] = self
        self._doc.filename = asksaveasfilename(**options)
        if not self._doc.filename:
            return
        self._onSave()
        
    def _onClose(self, event=None):
        """Fecha o documento"""
        if self.confirmDiscard():
            sys.exit()
            
    def _onPopupMenu(self, event):
        """Exibe o menu de popup"""
        if event.widget != self._doc.canvas:
            self._rsel = event.widget
        self._popupmenu.post(event.x_root, event.y_root)

        
    #=====================================================================
    # Drag and Drop
    #=====================================================================
    
    def _onMotion(self, event):
        """Movimento do drag-and-drop"""
        if (not self._app.drag) and (not self._select):
            return
        xrel, yrel = self._doc.getCoords(event)
        xrel = xrel if xrel > 0 else 0
        yrel = yrel if yrel > 0 else 0
        if self._app.drag:
            if self._highlight:
                self._doc.canvas.delete(self._highlight)
            self._highlight = self._doc.canvas.create_rectangle(xrel-5, yrel-5, 
                                                                xrel+5, yrel+5, 
                                                                fill='red')
        elif self._select:
            self._select.place(x=xrel, y=yrel, anchor=CENTER)
    
    def _onButtonRelease(self, event):
        """Final drag-and-drop"""
        if (not self._app.drag) and (not self._select):
            return
        #Limits
        xrel, yrel = self._doc.getCoords(event)
        if self._highlight:
            self._doc.canvas.delete(self._highlight)
        #Dragged item
        if self._app.drag and self._doc.isInside(xrel, yrel):
            className = event.widget.__class__
            new = className(self._doc)
            new.place(x=xrel, y=yrel, anchor=CENTER)
            self._configWidgetEvents(new)
            o = library[className]
            for p in o.properties:
                if not p.name.startswith('prop_'):
                    new[p.name] = p.defval
                else:
                    new.__setattr__(p.name, p.defval)
            self._doc += new
        #Delete
        if self._select and not self._doc.isInside(xrel, yrel):
            self._removeWidget(self._select)
        #Prepare for next drag-n-drop
        self._app.drag = None
        self._select = None #in case it wasn't None already
        self._doc.saved = False
        self._updateTitle()
            
    def _onObjectSelect(self, event):
        """Início do drag-and-drop de um elemento do desenho"""
        self._select = event.widget

    #=====================================================================
    # Widget events
    #=====================================================================        

    def _configWidgetEvents(self, widget):
        widget.bind('<ButtonPress-1>', self._onObjectSelect)
        widget.bind('<Double-Button-1>', self._onDoubleClick)
        widget.bind('<ButtonPress-3>', self._onPopupMenu)
        
    def _removeWidget(self, widget):
        self._doc -= widget
        widget.destroy()

    #=====================================================================
    # Property Editor
    #=====================================================================
    
    def _onDoubleClick(self, event=None, widget=None):
        """Exibição do diálogo de propriedades"""
        if widget is None:
            widget = event.widget
        prop = PropertyEditor(self, widget, library[widget.__class__])
        self.wait_window(prop)
        self._doc.saved = False
        self._updateTitle()
        
    def _onSetup(self, event=None):
        """Configura dimensões, plano de fundo e grade do documento"""
        prop = PropertyEditor(self, self._doc, self._doc)
        self.wait_window(prop)
        self._doc.updateCanvas()
        self._doc.saved = False
        self._updateTitle()
