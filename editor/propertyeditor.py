import sys

from tkinter import *
from tkinter.ttk import *

from core.types import *

from editor.library import library
from editor.property import *

from witkets.colorbutton import ColorButton
from witkets.filechooserentry import FileChooserEntry
from witkets.numericlabel import Bases

class EditorField:
    def __init__(self, cls=None, options=None, varcls=None, onecolumn=False):
        self.cls = cls
        if options:
            self.options = options
        else:
            self.options = {} # empty dictionary "default argument"
        self.varcls = varcls
        self.onecolumn = onecolumn
        
    def __str__(self):
        vals = (str(self.cls).split("'")[1].replace('tkinter', ''), 
                self.options, 
                str(self.varcls).split("'")[1].replace('tkinter', ''),
                self.onecolumn)
        return "EditorField(cls=%s options=%s varcls=%s onecolumn=%s)" % vals

propfields = {
    bool: EditorField(cls=Checkbutton, varcls=BooleanVar, onecolumn=True),
    pin: EditorField(cls=Spinbox, varcls=IntVar, options={'from': 0, 'to': 7}),
    str: EditorField(cls=Entry, varcls=StringVar),
    float: EditorField(cls=Entry, varcls=DoubleVar),
    color: EditorField(cls=ColorButton, varcls=StringVar),
    uint8: EditorField(cls=Spinbox, varcls=IntVar, 
                       options={'from': 0, 'to': 255}),
    uint16: EditorField(cls=Spinbox, varcls=IntVar, 
                        options={'from': 0, 'to': 65535}),
    action: EditorField(cls=Combobox, varcls=StringVar, 
                        options={'values': ['senddigital', 'sendanalog']}),
    image: EditorField(
        cls=FileChooserEntry, varcls=StringVar,
        options={'format': [ 
            ('Imagens GIF', '*.gif'), 
            ('Imagens JPG', '*.jpg'), 
            ('Imagens PNG', '*.png') ]}),
    inchs: EditorField(
        cls=Combobox, varcls=StringVar,
        options={'values': ['digital', 'an0', 'an1', 'an2', 'an3', 
                            'an4', 'an5', 'an6', 'an7']}),
    outchs: EditorField(
        cls=Combobox, varcls=StringVar,
        options={'values': ['digital', 'pwm0', 'pwm1', 'pwm2']}),
    direction: EditorField(
        cls=Combobox, varcls=StringVar,
        options={'values': ['entrada', 'saída']}),
    Bases: EditorField(
        cls=Combobox, varcls=StringVar,
        options={'values': ['BIN', 'DEC', 'HEX'] }
    )
}

class PropertyEditor(Toplevel):
    def __init__(self, parent, widget, libitem):
        Toplevel.__init__(self, parent)
        self.parent = parent
        self.transient(parent)
        self.title('Propriedades')
        self['bg'] = '#FFF'
        self.geometry("+%d+%d" % (widget.winfo_rootx()+50,
                                  widget.winfo_rooty()+50))
        #Core
        self.widget = widget
        self.libitem = libitem
        self.values = []
        #Editor and button boxes
        self._createForm()
        self._buttonBox()
    
    def _buttonBox(self):
        self.bind("<Return>", self.ok)
        self.bind("<Escape>", self.ok)
        self.protocol("WM_DELETE_WINDOW", self.ok)
        actionArea = Frame(self)
        actionArea.pack()
        b = Button(actionArea, text="OK", command=self.ok, default=ACTIVE)
        b.pack(fill=X, expand=1)
            
    def _createForm(self):
        formArea = Frame(self)
        formArea.pack(pady=20)
        if not self.libitem.columnbreaks:
            self._fillColumn(formArea, self.libitem.properties)
            return
        #Otherwise
        columnPoints = len(self.libitem.columnbreaks)
        idxA = 0
        for i in range(columnPoints):
            column = Frame(formArea)
            column.pack(padx=30, side=LEFT)
            idxB = self.libitem.columnbreaks[i]
            self._fillColumn(column, self.libitem.properties[idxA:idxB])
            idxA = idxB #incrementing cursor
        #Last column
        column = Frame(formArea)
        column.pack(side=LEFT)
        self._fillColumn(column, self.libitem.properties[idxA:])

            
    def _fillColumn(self, master, properties):
        row = 0
        for p in properties:
            if p.typename is None: #hidden property
                continue
            editorField = propfields[p.typename]
            inputcls = editorField.cls
            inputkw = editorField.options
            inputvar = editorField.varcls
            var = inputvar()
            if inputcls == Checkbutton:
                key = 'variable'
            else:
                key = 'textvariable'
            inputkw[key] = var
            
            field = inputcls(master, **inputkw)
            if row == 0:
                field.focus_set()
            if editorField.onecolumn: #FIXME one column or boolean var (or both)?
                field['text'] = p.label
                field.grid(row=row, column=0, columnspan=2, sticky=W, ipadx=30)
            else:
                Label(master, text=p.label).grid(row=row, column=0, sticky=E)
                field.grid(row=row, column=1, sticky=W)
            if p.name.startswith('prop_'):
                var.set(getattr(self.widget, p.name))
            else:
                var.set(self.widget[p.name])
            self.values.append(var)
            row += 1        
            
    def ok(self, event=None):
        i = 0
        for i in range(len(self.libitem.properties)):
            p = self.libitem.properties[i]
            if p.typename == None: #hidden property
                continue
            #val = self._getVal(p.type, self.values[i].get())
            val = self.values[i].get()
            if p.name.startswith('prop_'):
                setattr(self.widget, p.name, val)
            else:
                self.widget[p.name] = val
        self.destroy()
