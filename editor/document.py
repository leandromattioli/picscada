from tkinter import *
from tkinter.ttk import *

from core.types import *
from editor.library import library
from editor.property import *

from persistence.docwriter import DocWriter
from persistence.docreader import DocReader

import sys

class Document(Frame):

    def __init__(self, parent, **kw):
        if 'width' not in kw:
            kw['width'] = 640
        if 'height' not in kw:
            kw['height'] = 480
        Frame.__init__(self, parent, **kw)
        self._saved = True
        self._filename = ''
        self._initProps()
        self._initCanvas()
        
    def _initProps(self):
        self._properties = [
            Property(name='width', label='Largura:', typename=uint16),
            Property(name='height', label='Altura:', typename=uint16),
            Property(name='prop_bgimg', label='Imagem:', typename=image),
            Property(name='prop_gridstepx', label='Passo da Grade (X):', typename=uint16),
            Property(name='prop_gridstepy', label='Passo da Grade (Y):', typename=uint16),
            Property(name='prop_gridshow', label='Mostrar grade', typename=bool),
            Property(name='prop_gridsnap', label='Alinhar à grade', typename=bool)
        ]
        self.prop_bgimg = ''
        self.prop_gridstepx = self.prop_gridstepy = 25
        self.prop_gridshow = False
        self.prop_gridsnap = False
        
    def _initCanvas(self):
        self._img = None
        self._objects = []
        self._gridLines = []
        self._window = Canvas(self, width=self['width'], height=self['height'])
        self._window['bg'] = '#FFF'
        self._window.pack()

    #=====================================================================
    # Operators
    #=====================================================================        
    
    def __iadd__(self, obj): # doc += obj
        self._objects.append(obj)
        return self
        
    def __isub__(self, obj): # doc -= obj
        self._objects.remove(obj)
        return self

    def __setitem__(self, key, val): # doc[key] = val
        if key == 'width':
            self._window['width'] = val
        elif key == 'height':
            self._window['height'] = val
        Frame.__setitem__(self, key, val)
        
    def __iter__(self): #for o in doc
        return self._objects.__iter__()
    
    #=====================================================================
    # Properties
    #=====================================================================
    
    @property
    def properties(self):
        return self._properties
        
    @property
    def columnbreaks(self):
        return []
    
    @property
    def canvas(self):
        return self._window
    
    @property
    def saved(self):
        return self._saved
        
    @property
    def filename(self):
        return self._filename
    
    @saved.setter
    def saved(self, val):
        self._saved = True if val else False #ensuring boolean type
        
    @filename.setter
    def filename(self, val):
        self._filename = val
    
    #=====================================================================
    # Persistence
    #=====================================================================
    
    @staticmethod
    def fromFile(parent, filename):
        """Abre um documento a partir de um arquivo"""
        doc = Document(parent)
        doc.filename = filename
        reader = DocReader(doc)
        reader.buildFile(filename)
        for k,v in reader.nodes.items():
            doc += v
        doc.updateCanvas()
        return doc
    
    def save(self):
        """Saves the document"""
        writer = DocWriter(self)
        with open(self._filename, 'w') as f:
            writer.dump(f)
            self._saved = True
            
    #=====================================================================
    # Static Canvas (Grid and Image)
    #=====================================================================
    
    def updateCanvas(self):
        """Updates document drawing"""
        if self.prop_gridshow:
            self._drawGrid()
        else:
            for l in self._gridLines:
                self._window.delete(l)
            self._gridLines.clear()
        self._updateImage()
        
    def _drawGrid(self):
        for obj in self._gridLines:
            self._window.delete(obj)
        self._gridLines.clear()
        w, h = int(self._window['width']), int(self._window['height'])
        x = 0
        while x < w:
            line = self._window.create_line(x, 0, x, h, fill='#CCC')
            self._gridLines.append(line)
            x += self.prop_gridstepx
        y = 0
        while y < h:
            line = self._window.create_line(0, y, w, y, fill='#CCC')
            self._gridLines.append(line)
            y += self.prop_gridstepy

    def _updateImage(self):
        imgFile = self.prop_bgimg
        if self._img:
            self._window.delete(self._img)
        if not imgFile:
            return
        try:
            self.photoimg = PhotoImage(file=imgFile)
            self._img = self._window.create_image(0, 0, image=self.photoimg, anchor=NW)
        except TclError:
            sys.stderr.writelines(['Imagem %s não encontrada' % imgFile])
            return            
        
    #=====================================================================
    # Drag and Drop
    #=====================================================================
    
    def getCoords(self, event):
        """Obtém coordenadas ajustadas para o drag'n'drop"""
        xrel = event.x_root - self._window.winfo_rootx()
        yrel = event.y_root - self._window.winfo_rooty()
        if self.prop_gridsnap:
            xrel -= xrel % self.prop_gridstepx
            yrel -= yrel % self.prop_gridstepy
        return (xrel, yrel)
        
    def isInside(self, xrel, yrel):
        """Coordinates inside the Canvas?"""
        return xrel > 0 and yrel > 0 and \
               xrel < int(self['width']) and \
               yrel < int(self['height'])
