from collections import OrderedDict

from tkinter import *
from tkinter.ttk import *

from editor.libraryitem import LibraryItem
from editor.property import *

from witkets.led import LED
from witkets.logicswitch import LogicSwitch
from witkets.scope import Scope
from witkets.numericlabel import NumericLabel, Bases
from witkets.tank import Tank

library = OrderedDict()

library[Button] = LibraryItem(
    name='button', label='Botão', 
    properties = [
        Property(name='text', label='Texto:', typename=str, defval='Enviar'),
        Property(name='prop_cmd', label='Ação:', typename=action, 
                 defval='senddigital')
    ]
)

library[LogicSwitch] = LibraryItem(
    name='logicswitch', label='Chave',
    properties = [
        Property(name='prop_outpin', label='Pino (PORTD):', typename=pin, 
                 defval=0),
        Property(name='width', label='Largura:', typename=uint16, defval=25),
        Property(name='height', label='Altura:', typename=uint16, defval=25)
    ]
)

library[Entry] = LibraryItem(
    name='entry', label='Entrada',
    properties = [
        Property(name='prop_outchs', label='Tipo:', typename=outchs, 
                 defval='digital')
    ]
)

library[LED] = LibraryItem(
    name='led', label='LED',
    properties = [
        Property(name='prop_inpin', label='Pino (PORTB):', typename=pin, 
                 defval=0),
        Property(name='width', label='Largura:', typename=uint16, defval=25),
        Property(name='height', label='Altura:', typename=uint16, defval=25)
    ]
)

library[Scope] = LibraryItem(
    name='scope', label='Osciloscópio',
    properties = [
        Property(name='prop_inchs', label='Canal:', typename=inchs, 
                 defval='an0'),
        Property(name='plot_width', label='Largura:', typename=uint16, 
                 defval=100),
        Property(name='plot_height', label='Altura:', typename=uint16, 
                 defval=75),
        Property(name='plot_xscale', label='Escala (X):', 
                 typename=float, defval=1.0),
        Property(name='plot_yscale', label='Escala (Y):', 
                 typename=float, defval=1.0),
        Property(name='plot_xstep', label='Passo da Grade (X):', 
                 typename=float, defval=20.0),
        Property(name='plot_ystep', label='Passo da Grade (Y):', 
                 typename=float, defval=20.0),
        Property(name='plot_xoffset', label='Offset (X):', 
                 typename=float, defval=0.0),
        Property(name='plot_yoffset', label='Offset (Y):', 
                 typename=float, defval=0.0),
        Property(name='plot_padl', label='Padding (Esquerda):', 
                 typename=uint16, defval=30),
        Property(name='plot_padr', label='Padding (Direita):', 
                 typename=uint16, defval=10),
        Property(name='plot_padt', label='Padding (Acima):', 
                 typename=uint16, defval=10),
        Property(name='plot_padb', label='Padding (Abaixo):', 
                 typename=uint16, defval=20),
        Property(name='plot_colorgrid', label='Cor da Grade:', 
                 typename=color, defval='#CCC'),
        Property(name='plot_colorline', label='Cor do Traço:', 
                 typename=color, defval='#000'),
        Property(name='plot_tickfont', label='Fonte:',
                 typename=str, defval='"Courier New" 8'),
        Property(name='plot_autoscroll', label='Rolagem automática',
                 typename=bool, defval=True),
        Property(name='plot_showcircle', label='Destacar último ponto',
                 typename=bool, defval=True),
        Property(name='plot_xautorange', label='Escala automática (X)',
                 typename=bool, defval=False),
        Property(name='plot_yautorange', label='Escala automática (Y)',
                 typename=bool, defval=False),
        Property(name='style', label=None, typename=None, 
                 defval='Bordered.TFrame')
    ],
    columnbreaks = [ 9 ]
)

library[Label] = LibraryItem(
    name='label', label='Rótulo', 
    properties = [
        Property(name='text', label='Texto:', typename=str, defval='Texto')
    ]
)

library[NumericLabel] = LibraryItem(
    name='numericlabel', label='Rótulo Numérico', 
    properties = [
        Property(name='prop_inchs', label='Tipo:', typename=inchs, 
                 defval='digital'),
        Property(name='base', label='Base:', typename=Bases, defval=Bases.DEC)
    ]
)



library[Tank] = LibraryItem(
    name='labeledscale', label='Tanque',
    properties = [
        Property(name='width', label='Largura:', typename=uint16, defval=80),
        Property(name='height', label='Altura:', typename=uint16, defval=80),
        Property(name='colorlow', label='Cor (Nível Baixo):', typename=color, 
                 defval='red'),
        Property(name='colormedium', label='Cor (Nível Médio):', typename=color, 
                 defval='blue'),
        Property(name='colorhigh', label='Cor (Nível Alto):', typename=color, 
                 defval='green'),
        Property(name='padl', label='Padding (Esquerda)', typename=uint8, defval=30),
        Property(name='padr', label='Padding (Direita)', typename=uint8, defval=30),
        Property(name='padt', label='Padding (Acima)', typename=uint8, defval=10),
        Property(name='padb', label='Padding (Abaixo)', typename=uint8, defval=10),
        Property(name='tickfont', label='Fonte:', typename=str, 
                 defval='"Courier New" 8'),
        Property(name='levellow', label='Nível Baixo', typename=float, defval=20),
        Property(name='levelhigh', label='Nível Alto', typename=float, defval=80),
        Property(name='yscale', label='Escala:', typename=float, defval=1.0),
        Property(name='yoffset', label='Offset:', typename=float, defval=0.0),
        Property(name='ystep', label='Passo:', typename=float, defval=20.0),
        Property(name='prop_direction', label='Direção:', typename=direction, 
                 defval='entrada'),
        Property(name='prop_inchs', label='Entrada:', typename=inchs, 
                 defval='digital'),
        Property(name='prop_outchs', label='Saída:', typename=outchs, 
                 defval='digital')
    ],
    columnbreaks = [ 10 ]
)
