import sys
from cx_Freeze import setup, Executable

# python setup.py build

# Dependencies are automatically detected, but it might need fine tuning.
packages = ['os', 'tkinter', 'tkbuilder', 'persistence', 'witkets']
build_exe_options = {"packages": packages}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(  
    name = "picscada",
    version = "0.1",
    description = "PICScada",
    options = {"build_exe": build_exe_options},
    executables = [
        Executable("winmaker.py", base=base),
        Executable("winviewer.py", base=base)
    ]
)
